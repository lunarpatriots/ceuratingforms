﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmAdHome
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAdHome))
        Me.tmrDay = New System.Windows.Forms.Timer(Me.components)
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.lblUsername = New MetroFramework.Controls.MetroLabel()
        Me.lblTime = New MetroFramework.Controls.MetroLabel()
        Me.MetroPanel1 = New MetroFramework.Controls.MetroPanel()
        Me.lnkNotif = New MetroFramework.Controls.MetroLink()
        Me.tilLogout = New MetroFramework.Controls.MetroTile()
        Me.tilAccounts = New MetroFramework.Controls.MetroTile()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.tilList = New MetroFramework.Controls.MetroTile()
        Me.tilStudents = New MetroFramework.Controls.MetroTile()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.MetroPanel1.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tmrDay
        '
        Me.tmrDay.Enabled = True
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel2.Location = New System.Drawing.Point(207, 47)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(172, 25)
        Me.MetroLabel2.TabIndex = 11
        Me.MetroLabel2.Text = "Student Affairs Office"
        Me.MetroLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'MetroLabel1
        '
        Me.MetroLabel1.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel1.Location = New System.Drawing.Point(156, 311)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(260, 20)
        Me.MetroLabel1.TabIndex = 20
        Me.MetroLabel1.Text = "©BSIT3A 2016"
        Me.MetroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblUsername
        '
        Me.lblUsername.AutoSize = True
        Me.lblUsername.Location = New System.Drawing.Point(3, 5)
        Me.lblUsername.Name = "lblUsername"
        Me.lblUsername.Size = New System.Drawing.Size(64, 19)
        Me.lblUsername.TabIndex = 21
        Me.lblUsername.Text = "Welcome"
        Me.lblUsername.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTime
        '
        Me.lblTime.Location = New System.Drawing.Point(259, 5)
        Me.lblTime.Name = "lblTime"
        Me.lblTime.Size = New System.Drawing.Size(93, 19)
        Me.lblTime.TabIndex = 22
        Me.lblTime.Text = "Time"
        Me.lblTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'MetroPanel1
        '
        Me.MetroPanel1.BackColor = System.Drawing.Color.Transparent
        Me.MetroPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.MetroPanel1.Controls.Add(Me.lblUsername)
        Me.MetroPanel1.Controls.Add(Me.lblTime)
        Me.MetroPanel1.HorizontalScrollbarBarColor = True
        Me.MetroPanel1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.HorizontalScrollbarSize = 10
        Me.MetroPanel1.Location = New System.Drawing.Point(115, 114)
        Me.MetroPanel1.Name = "MetroPanel1"
        Me.MetroPanel1.Size = New System.Drawing.Size(357, 31)
        Me.MetroPanel1.Style = MetroFramework.MetroColorStyle.Pink
        Me.MetroPanel1.TabIndex = 27
        Me.MetroPanel1.UseStyleColors = True
        Me.MetroPanel1.VerticalScrollbarBarColor = True
        Me.MetroPanel1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.VerticalScrollbarSize = 10
        '
        'lnkNotif
        '
        Me.lnkNotif.Location = New System.Drawing.Point(115, 146)
        Me.lnkNotif.Name = "lnkNotif"
        Me.lnkNotif.Size = New System.Drawing.Size(357, 23)
        Me.lnkNotif.Style = MetroFramework.MetroColorStyle.Pink
        Me.lnkNotif.TabIndex = 29
        Me.lnkNotif.Text = "Notification"
        Me.lnkNotif.UseSelectable = True
        '
        'tilLogout
        '
        Me.tilLogout.ActiveControl = Nothing
        Me.tilLogout.Location = New System.Drawing.Point(405, 175)
        Me.tilLogout.Name = "tilLogout"
        Me.tilLogout.Size = New System.Drawing.Size(110, 110)
        Me.tilLogout.Style = MetroFramework.MetroColorStyle.Pink
        Me.tilLogout.TabIndex = 32
        Me.tilLogout.Text = "Logout"
        Me.tilLogout.Theme = MetroFramework.MetroThemeStyle.Light
        Me.tilLogout.TileImage = Global.ceuratingforms.My.Resources.Resources.ic_exit_to_app_white_48dp_1x
        Me.tilLogout.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.tilLogout.UseSelectable = True
        Me.tilLogout.UseTileImage = True
        '
        'tilAccounts
        '
        Me.tilAccounts.ActiveControl = Nothing
        Me.tilAccounts.Location = New System.Drawing.Point(289, 175)
        Me.tilAccounts.Name = "tilAccounts"
        Me.tilAccounts.Size = New System.Drawing.Size(110, 110)
        Me.tilAccounts.Style = MetroFramework.MetroColorStyle.Pink
        Me.tilAccounts.TabIndex = 31
        Me.tilAccounts.Text = "Settings"
        Me.tilAccounts.Theme = MetroFramework.MetroThemeStyle.Light
        Me.tilAccounts.TileImage = Global.ceuratingforms.My.Resources.Resources.ic_settings_white_48dp_1x
        Me.tilAccounts.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.tilAccounts.UseSelectable = True
        Me.tilAccounts.UseTileImage = True
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.ceuratingforms.My.Resources.Resources.ceu_sao
        Me.PictureBox2.Location = New System.Drawing.Point(459, 14)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(77, 77)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 30
        Me.PictureBox2.TabStop = False
        '
        'tilList
        '
        Me.tilList.ActiveControl = Nothing
        Me.tilList.Location = New System.Drawing.Point(173, 174)
        Me.tilList.Name = "tilList"
        Me.tilList.Size = New System.Drawing.Size(110, 110)
        Me.tilList.Style = MetroFramework.MetroColorStyle.Pink
        Me.tilList.TabIndex = 28
        Me.tilList.Text = "Reports"
        Me.tilList.Theme = MetroFramework.MetroThemeStyle.Light
        Me.tilList.TileImage = Global.ceuratingforms.My.Resources.Resources.ic_description_white_48dp_1x
        Me.tilList.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.tilList.UseSelectable = True
        Me.tilList.UseTileImage = True
        '
        'tilStudents
        '
        Me.tilStudents.ActiveControl = Nothing
        Me.tilStudents.Location = New System.Drawing.Point(57, 174)
        Me.tilStudents.Name = "tilStudents"
        Me.tilStudents.Size = New System.Drawing.Size(110, 110)
        Me.tilStudents.Style = MetroFramework.MetroColorStyle.Pink
        Me.tilStudents.TabIndex = 23
        Me.tilStudents.Text = "Students"
        Me.tilStudents.Theme = MetroFramework.MetroThemeStyle.Light
        Me.tilStudents.TileImage = CType(resources.GetObject("tilStudents.TileImage"), System.Drawing.Image)
        Me.tilStudents.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.tilStudents.UseSelectable = True
        Me.tilStudents.UseTileImage = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.ceuratingforms.My.Resources.Resources.ceu
        Me.PictureBox1.Location = New System.Drawing.Point(42, 14)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(77, 77)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 17
        Me.PictureBox1.TabStop = False
        '
        'frmAdHome
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(578, 343)
        Me.Controls.Add(Me.tilLogout)
        Me.Controls.Add(Me.tilAccounts)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.lnkNotif)
        Me.Controls.Add(Me.tilList)
        Me.Controls.Add(Me.MetroPanel1)
        Me.Controls.Add(Me.tilStudents)
        Me.Controls.Add(Me.MetroLabel1)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.MetroLabel2)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmAdHome"
        Me.Resizable = False
        Me.ShadowType = MetroFramework.Forms.MetroFormShadowType.DropShadow
        Me.Style = MetroFramework.MetroColorStyle.Pink
        Me.Text = "CENTRO ESCOLAR UNIVERSITY"
        Me.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center
        Me.MetroPanel1.ResumeLayout(False)
        Me.MetroPanel1.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tmrDay As Timer
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents lblUsername As MetroFramework.Controls.MetroLabel
    Friend WithEvents lblTime As MetroFramework.Controls.MetroLabel
    Friend WithEvents tilStudents As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroPanel1 As MetroFramework.Controls.MetroPanel
    Friend WithEvents tilList As MetroFramework.Controls.MetroTile
    Friend WithEvents lnkNotif As MetroFramework.Controls.MetroLink
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents tilAccounts As MetroFramework.Controls.MetroTile
    Friend WithEvents tilLogout As MetroFramework.Controls.MetroTile
End Class
