﻿Public Class frmAdHome

    Dim notifs As Integer

    Private Sub FrmAdHome_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Me.Closing
        If MsgBox("Are you sure you want to exit?", MsgBoxStyle.YesNo, "") = MsgBoxResult.Yes Then
            End
        Else
            e.Cancel = True
        End If
    End Sub

    Public Sub FetchNotifications()
        Dim query = "SELECT * FROM ratingform1 WHERE appr = 0"
        Dim query1 = "SELECT * FROM ratingform2 WHERE appr = 0"
        Dim query2 = "SELECT * FROM ratingform3 WHERE appr = 0"
        Dim query3 = "SELECT * FROM ratingform4 WHERE appr = 0"
        Dim query4 = "SELECT * FROM ratingform5 WHERE appr = 0"
        Dim query5 = "SELECT * FROM ratingform6 WHERE appr = 0"
        Dim query6 = "SELECT * FROM ratingform7 WHERE appr = 0"

        notifs = count(query, 1) + count(query1, 1) + count(query2, 1) + count(query3, 1) + count(query4, 1) + count(query5, 1) + count(query6, 1)
        lnkNotif.Visible = True
        lnkNotif.Text = "You have " & notifs & " rating forms waiting for approval."
        If notifs = 0 Then
            lnkNotif.Visible = False
        End If
    End Sub

    Private Sub frmAdHome_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblUsername.Text = "Welcome, " & GetAccount() & "!"
        If GetAccess() <> "Administrator" Then
            lnkNotif.Visible = False
        Else
            FetchNotifications()
        End If

    End Sub

    Private Sub tmrDay_Tick(sender As Object, e As EventArgs) Handles tmrDay.Tick
        lblTime.Text = Date.Now.ToString("hh:mm:ss tt")
    End Sub

    Private Sub tillogout_click(sender As Object, e As EventArgs)
        Select Case MsgBox("are you sure you want to log out?", MsgBoxStyle.YesNo, "")
            Case MsgBoxResult.Yes
                Dim nextform = New frmLogin()
                Hide()
                nextform.Show()
        End Select
    End Sub

    Private Sub tilStudents_Click(sender As Object, e As EventArgs) Handles tilStudents.Click
        Dim nextForm = New frmStudents()
        nextForm.Show()
        Hide()
    End Sub

    Private Sub tilAccounts_Click(sender As Object, e As EventArgs) Handles tilAccounts.Click
        Dim nextForm = New frmAdAccounts()
        nextForm.Show()
        Hide()
    End Sub

    Private Sub tilList_Click(sender As Object, e As EventArgs) Handles tilList.Click
        Dim nextForm = New frmReports()
        nextForm.Show()
        Hide()
    End Sub

    Private Sub lnkNotif_Click(sender As Object, e As EventArgs) Handles lnkNotif.Click
        If notifs > 0 Then
            Dim nextForm = New frmAForms()
            nextForm.init(Me)
            nextForm.Show()
            Enabled = False
        End If
    End Sub
End Class