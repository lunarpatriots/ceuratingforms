﻿Public Class frmReports
    Private Sub FrmReports_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Me.Closing
        If MsgBox("Are you sure you want to exit?", MsgBoxStyle.YesNo, "") = MsgBoxResult.Yes Then
            End
        Else
            e.Cancel = True
        End If
    End Sub
    Private Sub tilAll_Click(sender As Object, e As EventArgs) Handles tilAll.Click
        Dim nxtForm = New frmLStud()
        nxtForm.Show()
        Hide()
    End Sub

    Private Sub tilHome_Click(sender As Object, e As EventArgs) Handles tilHome.Click
        Dim nextForm As New frmAdHome()
        nextForm.Show()
        Hide()
    End Sub

    Private Sub tilStudents_Click(sender As Object, e As EventArgs) Handles tilStudents.Click
        Dim studReport As New studentReport
        studReport.Show()
        Hide()
    End Sub
End Class