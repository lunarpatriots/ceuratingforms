﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmLStud
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLStud))
        Dim ReportDataSource3 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.LScmbFilter = New MetroFramework.Controls.MetroComboBox()
        Me.MetroLabel4 = New MetroFramework.Controls.MetroLabel()
        Me.LScmbYG = New MetroFramework.Controls.MetroComboBox()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.PrintDialog1 = New System.Windows.Forms.PrintDialog()
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument()
        Me.PrintPreviewDialog1 = New System.Windows.Forms.PrintPreviewDialog()
        Me.MetroLabel5 = New MetroFramework.Controls.MetroLabel()
        Me.lstStudcmCS = New MetroFramework.Controls.MetroComboBox()
        Me.StudsumTableAdapter = New ceuratingforms.DataSet1TableAdapters.studsumTableAdapter()
        Me.ReportViewer2 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.StudSums = New ceuratingforms.StudSums()
        Me.StudsumBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.tilHome = New MetroFramework.Controls.MetroTile()
        Me.tilPrint = New MetroFramework.Controls.MetroTile()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.MetroLabel6 = New MetroFramework.Controls.MetroLabel()
        CType(Me.StudSums, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StudsumBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MetroLabel3
        '
        Me.MetroLabel3.Location = New System.Drawing.Point(357, 71)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(110, 19)
        Me.MetroLabel3.TabIndex = 19
        Me.MetroLabel3.Text = "Mendiola, Manila"
        Me.MetroLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'MetroLabel2
        '
        Me.MetroLabel2.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel2.Location = New System.Drawing.Point(326, 47)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(172, 25)
        Me.MetroLabel2.TabIndex = 18
        Me.MetroLabel2.Text = "Student Affairs Office"
        Me.MetroLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Location = New System.Drawing.Point(33, 109)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(38, 19)
        Me.MetroLabel1.TabIndex = 21
        Me.MetroLabel1.Text = "Filter"
        '
        'LScmbFilter
        '
        Me.LScmbFilter.FormattingEnabled = True
        Me.LScmbFilter.ItemHeight = 23
        Me.LScmbFilter.Items.AddRange(New Object() {"University-Wide", "College/School-Wide"})
        Me.LScmbFilter.Location = New System.Drawing.Point(90, 106)
        Me.LScmbFilter.Name = "LScmbFilter"
        Me.LScmbFilter.Size = New System.Drawing.Size(267, 29)
        Me.LScmbFilter.TabIndex = 22
        Me.LScmbFilter.UseSelectable = True
        '
        'MetroLabel4
        '
        Me.MetroLabel4.AutoSize = True
        Me.MetroLabel4.Location = New System.Drawing.Point(492, 106)
        Me.MetroLabel4.Name = "MetroLabel4"
        Me.MetroLabel4.Size = New System.Drawing.Size(119, 19)
        Me.MetroLabel4.TabIndex = 23
        Me.MetroLabel4.Text = "Year of Graduation"
        '
        'LScmbYG
        '
        Me.LScmbYG.FormattingEnabled = True
        Me.LScmbYG.ItemHeight = 23
        Me.LScmbYG.Location = New System.Drawing.Point(617, 100)
        Me.LScmbYG.Name = "LScmbYG"
        Me.LScmbYG.Size = New System.Drawing.Size(155, 29)
        Me.LScmbYG.TabIndex = 24
        Me.LScmbYG.UseSelectable = True
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.Size = New System.Drawing.Size(396, 246)
        Me.ReportViewer1.TabIndex = 0
        '
        'PrintDialog1
        '
        Me.PrintDialog1.Document = Me.PrintDocument1
        Me.PrintDialog1.UseEXDialog = True
        '
        'PrintPreviewDialog1
        '
        Me.PrintPreviewDialog1.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog1.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog1.ClientSize = New System.Drawing.Size(400, 300)
        Me.PrintPreviewDialog1.Document = Me.PrintDocument1
        Me.PrintPreviewDialog1.Enabled = True
        Me.PrintPreviewDialog1.Icon = CType(resources.GetObject("PrintPreviewDialog1.Icon"), System.Drawing.Icon)
        Me.PrintPreviewDialog1.Name = "PrintPreviewDialog1"
        Me.PrintPreviewDialog1.Visible = False
        '
        'MetroLabel5
        '
        Me.MetroLabel5.AutoSize = True
        Me.MetroLabel5.Location = New System.Drawing.Point(118, 146)
        Me.MetroLabel5.Name = "MetroLabel5"
        Me.MetroLabel5.Size = New System.Drawing.Size(98, 19)
        Me.MetroLabel5.TabIndex = 30
        Me.MetroLabel5.Text = "College/School"
        Me.MetroLabel5.Visible = False
        '
        'lstStudcmCS
        '
        Me.lstStudcmCS.FormattingEnabled = True
        Me.lstStudcmCS.ItemHeight = 23
        Me.lstStudcmCS.Items.AddRange(New Object() {"School of Accountancy and Management", "School of Dentistry", "School of Education, Liberal Arts, Music, and Social Work", "College of Medical Technology", "College of Nursing", "College of Optometry", "School of Pharmacy", "School of Science and Technology", "School of Nutrition and Hospitality Management"})
        Me.lstStudcmCS.Location = New System.Drawing.Point(222, 141)
        Me.lstStudcmCS.Name = "lstStudcmCS"
        Me.lstStudcmCS.Size = New System.Drawing.Size(499, 29)
        Me.lstStudcmCS.TabIndex = 31
        Me.lstStudcmCS.UseSelectable = True
        Me.lstStudcmCS.Visible = False
        '
        'StudsumTableAdapter
        '
        Me.StudsumTableAdapter.ClearBeforeFill = True
        '
        'ReportViewer2
        '
        ReportDataSource3.Name = "DataSet1"
        ReportDataSource3.Value = Me.StudsumBindingSource
        Me.ReportViewer2.LocalReport.DataSources.Add(ReportDataSource3)
        Me.ReportViewer2.LocalReport.ReportEmbeddedResource = "ceuratingforms.Report5.rdlc"
        Me.ReportViewer2.Location = New System.Drawing.Point(49, 141)
        Me.ReportViewer2.Name = "ReportViewer2"
        Me.ReportViewer2.Size = New System.Drawing.Size(769, 458)
        Me.ReportViewer2.TabIndex = 32
        '
        'StudSums
        '
        Me.StudSums.DataSetName = "StudSums"
        Me.StudSums.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'tilHome
        '
        Me.tilHome.ActiveControl = Nothing
        Me.tilHome.Location = New System.Drawing.Point(743, 605)
        Me.tilHome.Name = "tilHome"
        Me.tilHome.Size = New System.Drawing.Size(75, 75)
        Me.tilHome.Style = MetroFramework.MetroColorStyle.Pink
        Me.tilHome.TabIndex = 27
        Me.tilHome.Text = "Home"
        Me.tilHome.TileImage = Global.ceuratingforms.My.Resources.Resources.ic_home_white_48dp_1x
        Me.tilHome.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.tilHome.UseSelectable = True
        Me.tilHome.UseTileImage = True
        '
        'tilPrint
        '
        Me.tilPrint.ActiveControl = Nothing
        Me.tilPrint.Location = New System.Drawing.Point(662, 605)
        Me.tilPrint.Name = "tilPrint"
        Me.tilPrint.Size = New System.Drawing.Size(75, 75)
        Me.tilPrint.Style = MetroFramework.MetroColorStyle.Pink
        Me.tilPrint.TabIndex = 25
        Me.tilPrint.Text = "Print"
        Me.tilPrint.TileImage = Global.ceuratingforms.My.Resources.Resources.ic_print_white_48dp_1x
        Me.tilPrint.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.tilPrint.UseSelectable = True
        Me.tilPrint.UseTileImage = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.ceuratingforms.My.Resources.Resources.ceu
        Me.PictureBox1.Location = New System.Drawing.Point(148, 14)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(77, 77)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 20
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.ceuratingforms.My.Resources.Resources.ceu_sao
        Me.PictureBox2.Location = New System.Drawing.Point(617, 14)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(77, 77)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 33
        Me.PictureBox2.TabStop = False
        '
        'MetroLabel6
        '
        Me.MetroLabel6.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel6.Location = New System.Drawing.Point(290, 690)
        Me.MetroLabel6.Name = "MetroLabel6"
        Me.MetroLabel6.Size = New System.Drawing.Size(260, 20)
        Me.MetroLabel6.TabIndex = 34
        Me.MetroLabel6.Text = "©BSIT3A 2016"
        Me.MetroLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmLStud
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(841, 716)
        Me.Controls.Add(Me.MetroLabel6)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.lstStudcmCS)
        Me.Controls.Add(Me.MetroLabel5)
        Me.Controls.Add(Me.ReportViewer2)
        Me.Controls.Add(Me.tilHome)
        Me.Controls.Add(Me.tilPrint)
        Me.Controls.Add(Me.LScmbYG)
        Me.Controls.Add(Me.MetroLabel4)
        Me.Controls.Add(Me.LScmbFilter)
        Me.Controls.Add(Me.MetroLabel1)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.MetroLabel3)
        Me.Controls.Add(Me.MetroLabel2)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmLStud"
        Me.Resizable = False
        Me.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Style = MetroFramework.MetroColorStyle.Pink
        Me.Text = "CENTRO ESCOLAR UNIVERSITY"
        Me.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center
        CType(Me.StudSums, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StudsumBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents LScmbFilter As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroLabel4 As MetroFramework.Controls.MetroLabel
    Friend WithEvents LScmbYG As MetroFramework.Controls.MetroComboBox
    Private WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents tilPrint As MetroFramework.Controls.MetroTile
    Friend WithEvents tilHome As MetroFramework.Controls.MetroTile
    Friend WithEvents PrintDialog1 As PrintDialog
    Friend WithEvents PrintDocument1 As Printing.PrintDocument
    Friend WithEvents PrintPreviewDialog1 As PrintPreviewDialog
    Friend WithEvents MetroLabel5 As MetroFramework.Controls.MetroLabel
    Friend WithEvents lstStudcmCS As MetroFramework.Controls.MetroComboBox
    Friend WithEvents StudsumBindingSource As BindingSource
    Friend WithEvents StudsumTableAdapter As DataSet1TableAdapters.studsumTableAdapter
    Friend WithEvents StudSums As StudSums
    Friend WithEvents ReportViewer2 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents MetroLabel6 As MetroFramework.Controls.MetroLabel
End Class
