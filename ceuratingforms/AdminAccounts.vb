﻿Public Class frmAdAccounts

    Dim metroPink As Color = Color.FromArgb(230, 113, 184)
    Dim customBrush As Brush = New SolidBrush(metroPink)

    Dim command As String

    Private Sub FrmAdHome_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Me.Closing
        If MsgBox("Are you sure you want to exit?", MsgBoxStyle.YesNo, "") = MsgBoxResult.Yes Then
            End
        Else
            e.Cancel = True
        End If
    End Sub

    Public Sub RunCommand(ByRef command As String)
        Dim query = command
        Dim msg As String

        If ExecuteQuery(query) Then
            If query.Contains("DELETE") Then
                msg = "deleted!"
                DisplayAccounts()
            ElseIf query.Contains("UPDATE") Then
                msg = "updated!"
            End If
            MsgBox("Account " & msg)
            Reset()
        Else
            If query.Contains("DELETE") Then
                msg = "deleted!"
            ElseIf query.Contains("UPDATE") Then
                msg = "updated!"
            End If
            MsgBox("Account not " & msg)
        End If
    End Sub

    Private Sub lstAccounts_DrawItem(ByVal sender As Object, ByVal e As System.Windows.Forms.DrawItemEventArgs)
        e.DrawBackground()
        If (e.State And DrawItemState.Selected) = DrawItemState.Selected Then
            e.Graphics.FillRectangle(customBrush, e.Bounds)

        End If
        Using b As New SolidBrush(e.ForeColor)
            e.Graphics.DrawString(lstAccounts.GetItemText(lstAccounts.Items(e.Index)), e.Font, b, e.Bounds)
        End Using
        e.DrawFocusRectangle()
    End Sub

    Private Sub DisplayAccounts()
        lstAccounts.SelectedIndex = -1
        lstAccounts.Items.Clear()

        Dim query = "SELECT Username FROM userinfo"

        Dim accountList As ArrayList = RetrieveQuery(query, 1)

        For Each account As ArrayList In accountList
            lstAccounts.Items.Add(account(0).ToString())
        Next
    End Sub

    Private Sub frmAdAccounts_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        DisplayAccounts()
        loadpoints()
    End Sub

    Private Sub lstAccounts_SelectedIndexChanged(sender As Object, e As EventArgs)

        If lstAccounts.SelectedIndex <> -1 Then
            Dim username = lstAccounts.SelectedItem.ToString

            Dim query = "SELECT Username, authorization FROM userinfo WHERE Username = '" & username & "'"
            Dim account As ArrayList = RetrieveQuery(query, 2)

            txtUser.Text = account(0)(0).ToString()
            comType.SelectedIndex = comType.Items.IndexOf(account(0)(1).ToString())

            btnEdit.Enabled = True
            btnDelete.Enabled = True
        Else
            btnEdit.Enabled = False
            btnDelete.Enabled = False
            Clear()
        End If
    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs)
        btnEdit.Visible = False
        btnDelete.Visible = False
        btnSave.Visible = True
        btnCancel.Visible = True
        lstAccounts.Enabled = False
        txtUser.Enabled = True
        txtPassword.Enabled = True
        comType.Enabled = True

        txtUser.Select()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs)
        Reset()
    End Sub

    Private Sub Reset()
        btnEdit.Visible = True
        btnDelete.Visible = True
        btnSave.Visible = False
        btnCancel.Visible = False
        lstAccounts.Enabled = True

        txtPassword.Enabled = False
        comType.Enabled = False
    End Sub

    Private Sub Clear()
        txtUser.Text = String.Empty
        txtPassword.Text = String.Empty
        comType.SelectedIndex = -1
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs)
        Dim user As String = txtUser.Text

        If GetAccount() = user Then
            MsgBox("You cannot delete your own account!")
        Else
            If MsgBox("Are you sure you want to delete this account?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then


                Dim dialog = New diaPassword()
                Dim testQuery = "SELECT username,psword FROM userinfo WHERE Username = '" & GetAccount() & "' AND psword = '"
                Dim query = "DELETE FROM userinfo WHERE username = '" & user & "'"

                dialog.Init(Me, testQuery, query)
                dialog.Show()
                Me.Enabled = False
            End If
        End If
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs)
        If String.IsNullOrEmpty(txtUser.Text) Then
            tipError.Show("Please enter your new username", txtUser)
            txtUser.Focus()
        End If
        If String.IsNullOrEmpty(txtPassword.Text) Then
            tipError.Show("Please enter new password", txtPassword)
            txtPassword.Focus()
        End If

        Dim user As String = txtUser.Text
        Dim password As String = txtPassword.Text
        Dim type As String = comType.SelectedItem.ToString()

        Dim dialog = New diaPassword()

        Dim testQuery = "SELECT username, psword FROM userinfo WHERE Username = '" & user & "' AND psword = '"
        Dim query = "UPDATE userinfo SET psword = '" & password & "' , authorization = '" & type & "' WHERE Username = '" & user & "'"

        dialog.Init(Me, testQuery, query)
        dialog.Show()
        Me.Enabled = False
    End Sub

    Private Sub tilHome_Click(sender As Object, e As EventArgs)
        Dim nextForm = New frmAdHome
        nextForm.Show()
        Hide()
    End Sub

    Private Sub tilAdd_Click(sender As Object, e As EventArgs)
        Dim nextForm = New frmAdAccount
        nextForm.init(Me)
        nextForm.Show()
        Enabled = False
    End Sub
    Private Sub loadpoints()
        Dim ct As Integer
        Dim query
        query = "Select minPoint from ceuratingforms.points_ranges where award = 'Gold'; "
        ct = count(query, 1)
        If ct <> 0 Then
            Dim geld = RetrieveQuery(query, 1)
            txtGold.Text = geld(0)(0).ToString
        End If
        query = "Select minPoint from ceuratingforms.points_ranges where award = 'Silver'; "
        ct = count(query, 1)
        If ct <> 0 Then
            Dim sil = RetrieveQuery(query, 1)
            txtSilver.Text = sil(0)(0).ToString
        End If
        query = "Select minPoint from ceuratingforms.points_ranges where award = 'Bronze'; "
        ct = count(query, 1)
        If ct <> 0 Then
            Dim bro = RetrieveQuery(query, 1)
            txtBronze.Text = bro(0)(0).ToString
        End If
    End Sub

    Private Sub RBtn_Edit_Click(sender As Object, e As EventArgs) Handles RBtn_Edit.Click
        txtGold.Enabled = True
        txtSilver.Enabled = True
        txtBronze.Enabled = True
        RBtn_Edit.Visible = False
        RBtn_Save.Visible = True
    End Sub

    Private Sub RBtn_Save_Click(sender As Object, e As EventArgs) Handles RBtn_Save.Click
        txtBronze.Enabled = False
        txtGold.Enabled = False
        txtSilver.Enabled = False
        RBtn_Edit.Visible = True
        RBtn_Save.Visible = False

        Dim query

        If String.IsNullOrEmpty(txtGold.Text) Then
            tipError.Show("Please enter the minimum point.", txtGold)
            txtGold.Select()
        ElseIf String.IsNullOrEmpty(txtSilver.Text) Then
            tipError.Show("Please enter the minimum point.", txtSilver)
            txtSilver.Select()
        ElseIf String.IsNullOrEmpty(txtBronze.Text) Then
            tipError.Show("Please enter the minimum point.", txtBronze)
            txtBronze.Select()
        Else
            Dim goldp As Integer = Val(txtGold.Text)
            Dim silp As Integer = Val(txtSilver.Text)
            Dim brop As Integer = Val(txtBronze.Text)
            Dim question = MsgBox("Are you sure you want to save this?", "CEU Rating Forms Management System", MsgBoxStyle.YesNo)
            If question = MsgBoxResult.Yes Then
                query = "UPDATE ceuratingforms.points_ranges SET minPoint = '" & goldp & "' WHERE award = 'Gold'; "
                ExecuteQuery(query)

                query = "UPDATE ceuratingforms.points_ranges SET minPoint = '" & silp & "' WHERE award = 'Silver'; "
                ExecuteQuery(query)

                query = "UPDATE ceuratingforms.points_ranges SET minPoint = '" & brop & "' WHERE award = 'Bronze'; "
                ExecuteQuery(query)

                MsgBox("Changes saved successfully!", "CEU Rating Forms Management System")

                UpdatePointsInfo()
            ElseIf question = MsgBoxResult.No Then
                MsgBox("Changes were not saved", "CEU Rating Forms Management System")
            End If
        End If
    End Sub
    Private Sub UpdatePointsInfo()
        Dim query
        query = "SELECT award, minPoint FROM ceuratingforms.points_ranges"

        If count(query, 2) <> 0 Then
            Dim minPoints As ArrayList = RetrieveQuery(query, 2)
            Dim batchQuery As ArrayList = New ArrayList

            query = "SELECT StudNo, TotalPoints FROM ceuratingforms.pointsinfo"

            If count(query, 2) Then
                Dim allPoints As ArrayList = RetrieveQuery(query, 2)

                For Each points As ArrayList In allPoints
                    Dim studNo As String = points(0).ToString
                    Dim point As Integer = Integer.Parse(points(1).ToString)
                    Dim remarks As String = String.Empty

                    Dim minGold = minPoints(0)(1)
                    Dim minSilver = minPoints(1)(1)
                    Dim minBronze = minPoints(2)(1)

                    If point >= minGold Then
                        remarks = minPoints(0)(0)
                    ElseIf point >= minSilver Then
                        remarks = minPoints(1)(0)
                    ElseIf point >= minBronze Then
                        remarks = minPoints(2)(0)
                    End If

                    batchQuery.Add("UPDATE pointsinfo SET remarks = '" & remarks & "' WHERE StudNo = '" & studNo & "'")
                Next
            End If

            If batchQuery.Count() > 0 Then
                For Each query In batchQuery
                    ExecuteQuery(query)
                Next
            End If
        End If
    End Sub

    Private Sub RBtn_Cancel_Click(sender As Object, e As EventArgs) Handles RBtn_Cancel.Click
        loadpoints()
        txtGold.Enabled = False
        txtSilver.Enabled = False
        txtBronze.Enabled = False
        RBtn_Edit.Visible = True
        RBtn_Save.Visible = False
    End Sub
End Class