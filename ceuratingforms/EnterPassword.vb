﻿Public Class diaPassword

    Dim form As frmAdAccounts
    Dim query As String
    Dim com As String

    Public Sub Init(ByVal previousForm As Form, ByVal testQuery As String, ByVal command As String)
        form = previousForm
        query = testQuery
        com = command

        If command.Contains("DELETE") Then
            lblHeader.Text = "ENTER YOUR PASSWORD"
        ElseIf command.Contains("UPDATE") Then
            lblHeader.Text = "ENTER OLD PASSWORD"
        End If
    End Sub

    Private Sub diaPassword_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtPassword.Select()
    End Sub

    Private Sub btnOk_Click(sender As Object, e As EventArgs) Handles btnOk.Click
        If String.IsNullOrEmpty(txtPassword.Text) Then
            tipError.Show("Please enter password.", txtPassword)
            txtPassword.Select()
        Else
            Dim password = txtPassword.Text
            query &= password & "'"

            If count(query, 2) <> 0 Then
                form.Enabled = True
                form.RunCommand(com)
                Hide()
            Else
                MsgBox("Wrong password!")
                txtPassword.Select()
            End If
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        form.Enabled = True
        Hide()
    End Sub
End Class