﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAdAccounts
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAdAccounts))
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel7 = New MetroFramework.Controls.MetroLabel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.tipError = New MetroFramework.Components.MetroToolTip()
        Me.CustomTab1 = New ceuratingforms.CustomTab()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.tilAdd = New MetroFramework.Controls.MetroTile()
        Me.MetroPanel1 = New MetroFramework.Controls.MetroPanel()
        Me.MetroLabel6 = New MetroFramework.Controls.MetroLabel()
        Me.btnCancel = New MetroFramework.Controls.MetroButton()
        Me.btnSave = New MetroFramework.Controls.MetroButton()
        Me.btnDelete = New MetroFramework.Controls.MetroButton()
        Me.btnEdit = New MetroFramework.Controls.MetroButton()
        Me.MetroLabel5 = New MetroFramework.Controls.MetroLabel()
        Me.comType = New MetroFramework.Controls.MetroComboBox()
        Me.MetroLabel4 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.txtPassword = New MetroFramework.Controls.MetroTextBox()
        Me.txtUser = New MetroFramework.Controls.MetroTextBox()
        Me.lstAccounts = New System.Windows.Forms.ListBox()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.RBtn_Cancel = New MetroFramework.Controls.MetroButton()
        Me.txtBronze = New MetroFramework.Controls.MetroTextBox()
        Me.txtSilver = New MetroFramework.Controls.MetroTextBox()
        Me.txtGold = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel11 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel10 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel9 = New MetroFramework.Controls.MetroLabel()
        Me.RBtn_Edit = New MetroFramework.Controls.MetroButton()
        Me.RBtn_Save = New MetroFramework.Controls.MetroButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.MetroLabel8 = New MetroFramework.Controls.MetroLabel()
        Me.tilHome = New MetroFramework.Controls.MetroTile()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CustomTab1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.MetroPanel1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.Location = New System.Drawing.Point(329, 69)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(110, 19)
        Me.MetroLabel3.TabIndex = 28
        Me.MetroLabel3.Text = "Mendiola, Manila"
        Me.MetroLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel2.Location = New System.Drawing.Point(293, 48)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(172, 25)
        Me.MetroLabel2.TabIndex = 27
        Me.MetroLabel2.Text = "Student Affairs Office"
        Me.MetroLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'MetroLabel7
        '
        Me.MetroLabel7.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel7.Location = New System.Drawing.Point(255, 538)
        Me.MetroLabel7.Name = "MetroLabel7"
        Me.MetroLabel7.Size = New System.Drawing.Size(260, 20)
        Me.MetroLabel7.TabIndex = 31
        Me.MetroLabel7.Text = "©BSIT3A 2016"
        Me.MetroLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.ceuratingforms.My.Resources.Resources.ceu
        Me.PictureBox1.Location = New System.Drawing.Point(113, 13)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(75, 75)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 29
        Me.PictureBox1.TabStop = False
        '
        'tipError
        '
        Me.tipError.Style = MetroFramework.MetroColorStyle.Blue
        Me.tipError.StyleManager = Nothing
        Me.tipError.Theme = MetroFramework.MetroThemeStyle.Light
        '
        'CustomTab1
        '
        Me.CustomTab1.Alignment = System.Windows.Forms.TabAlignment.Left
        Me.CustomTab1.Controls.Add(Me.TabPage1)
        Me.CustomTab1.Controls.Add(Me.TabPage2)
        Me.CustomTab1.ItemSize = New System.Drawing.Size(44, 136)
        Me.CustomTab1.Location = New System.Drawing.Point(46, 119)
        Me.CustomTab1.Multiline = True
        Me.CustomTab1.Name = "CustomTab1"
        Me.CustomTab1.SelectedIndex = 0
        Me.CustomTab1.Size = New System.Drawing.Size(678, 370)
        Me.CustomTab1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed
        Me.CustomTab1.TabIndex = 32
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.White
        Me.TabPage1.Controls.Add(Me.tilAdd)
        Me.TabPage1.Controls.Add(Me.MetroPanel1)
        Me.TabPage1.Controls.Add(Me.lstAccounts)
        Me.TabPage1.Location = New System.Drawing.Point(140, 4)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(534, 362)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Accounts"
        '
        'tilAdd
        '
        Me.tilAdd.ActiveControl = Nothing
        Me.tilAdd.Location = New System.Drawing.Point(447, 19)
        Me.tilAdd.Name = "tilAdd"
        Me.tilAdd.Size = New System.Drawing.Size(75, 75)
        Me.tilAdd.Style = MetroFramework.MetroColorStyle.Pink
        Me.tilAdd.TabIndex = 37
        Me.tilAdd.Text = "New"
        Me.tilAdd.TileImage = Global.ceuratingforms.My.Resources.Resources.ic_person_add_white_48dp_1x
        Me.tilAdd.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.tilAdd.UseSelectable = True
        Me.tilAdd.UseTileImage = True
        '
        'MetroPanel1
        '
        Me.MetroPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.MetroPanel1.Controls.Add(Me.MetroLabel6)
        Me.MetroPanel1.Controls.Add(Me.btnCancel)
        Me.MetroPanel1.Controls.Add(Me.btnSave)
        Me.MetroPanel1.Controls.Add(Me.btnDelete)
        Me.MetroPanel1.Controls.Add(Me.btnEdit)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel5)
        Me.MetroPanel1.Controls.Add(Me.comType)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel4)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel1)
        Me.MetroPanel1.Controls.Add(Me.txtPassword)
        Me.MetroPanel1.Controls.Add(Me.txtUser)
        Me.MetroPanel1.HorizontalScrollbarBarColor = True
        Me.MetroPanel1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.HorizontalScrollbarSize = 10
        Me.MetroPanel1.Location = New System.Drawing.Point(173, 100)
        Me.MetroPanel1.Name = "MetroPanel1"
        Me.MetroPanel1.Size = New System.Drawing.Size(349, 199)
        Me.MetroPanel1.TabIndex = 35
        Me.MetroPanel1.VerticalScrollbarBarColor = True
        Me.MetroPanel1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.VerticalScrollbarSize = 10
        '
        'MetroLabel6
        '
        Me.MetroLabel6.AutoSize = True
        Me.MetroLabel6.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel6.Location = New System.Drawing.Point(109, 15)
        Me.MetroLabel6.Name = "MetroLabel6"
        Me.MetroLabel6.Size = New System.Drawing.Size(128, 19)
        Me.MetroLabel6.TabIndex = 12
        Me.MetroLabel6.Text = "ACCOUNT DETAILS"
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(177, 159)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 11
        Me.btnCancel.Text = "CANCEL"
        Me.btnCancel.UseSelectable = True
        Me.btnCancel.Visible = False
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(95, 159)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 23)
        Me.btnSave.TabIndex = 10
        Me.btnSave.Text = "SAVE"
        Me.btnSave.UseSelectable = True
        Me.btnSave.Visible = False
        '
        'btnDelete
        '
        Me.btnDelete.Enabled = False
        Me.btnDelete.Location = New System.Drawing.Point(177, 158)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(75, 23)
        Me.btnDelete.TabIndex = 9
        Me.btnDelete.Text = "DELETE"
        Me.btnDelete.UseSelectable = True
        '
        'btnEdit
        '
        Me.btnEdit.Enabled = False
        Me.btnEdit.Location = New System.Drawing.Point(95, 159)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(75, 23)
        Me.btnEdit.TabIndex = 8
        Me.btnEdit.Text = "EDIT"
        Me.btnEdit.UseSelectable = True
        '
        'MetroLabel5
        '
        Me.MetroLabel5.AutoSize = True
        Me.MetroLabel5.Location = New System.Drawing.Point(63, 106)
        Me.MetroLabel5.Name = "MetroLabel5"
        Me.MetroLabel5.Size = New System.Drawing.Size(87, 19)
        Me.MetroLabel5.TabIndex = 7
        Me.MetroLabel5.Text = "Account Type"
        '
        'comType
        '
        Me.comType.Enabled = False
        Me.comType.FormattingEnabled = True
        Me.comType.ItemHeight = 23
        Me.comType.Items.AddRange(New Object() {"Administrator", "Student Assistant"})
        Me.comType.Location = New System.Drawing.Point(162, 106)
        Me.comType.Name = "comType"
        Me.comType.Size = New System.Drawing.Size(121, 29)
        Me.comType.TabIndex = 6
        Me.comType.UseSelectable = True
        '
        'MetroLabel4
        '
        Me.MetroLabel4.AutoSize = True
        Me.MetroLabel4.Location = New System.Drawing.Point(64, 77)
        Me.MetroLabel4.Name = "MetroLabel4"
        Me.MetroLabel4.Size = New System.Drawing.Size(93, 19)
        Me.MetroLabel4.TabIndex = 5
        Me.MetroLabel4.Text = "New Password"
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Location = New System.Drawing.Point(64, 50)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(68, 19)
        Me.MetroLabel1.TabIndex = 4
        Me.MetroLabel1.Text = "Username"
        '
        'txtPassword
        '
        '
        '
        '
        Me.txtPassword.CustomButton.Image = Nothing
        Me.txtPassword.CustomButton.Location = New System.Drawing.Point(98, 1)
        Me.txtPassword.CustomButton.Name = ""
        Me.txtPassword.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.txtPassword.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtPassword.CustomButton.TabIndex = 1
        Me.txtPassword.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtPassword.CustomButton.UseSelectable = True
        Me.txtPassword.CustomButton.Visible = False
        Me.txtPassword.Enabled = False
        Me.txtPassword.Lines = New String(-1) {}
        Me.txtPassword.Location = New System.Drawing.Point(162, 77)
        Me.txtPassword.MaxLength = 32767
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(8226)
        Me.txtPassword.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtPassword.SelectedText = ""
        Me.txtPassword.SelectionLength = 0
        Me.txtPassword.SelectionStart = 0
        Me.txtPassword.ShortcutsEnabled = True
        Me.txtPassword.Size = New System.Drawing.Size(120, 23)
        Me.txtPassword.TabIndex = 3
        Me.txtPassword.UseSelectable = True
        Me.txtPassword.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtPassword.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'txtUser
        '
        '
        '
        '
        Me.txtUser.CustomButton.Image = Nothing
        Me.txtUser.CustomButton.Location = New System.Drawing.Point(98, 1)
        Me.txtUser.CustomButton.Name = ""
        Me.txtUser.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.txtUser.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtUser.CustomButton.TabIndex = 1
        Me.txtUser.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtUser.CustomButton.UseSelectable = True
        Me.txtUser.CustomButton.Visible = False
        Me.txtUser.Enabled = False
        Me.txtUser.Lines = New String(-1) {}
        Me.txtUser.Location = New System.Drawing.Point(162, 47)
        Me.txtUser.MaxLength = 32767
        Me.txtUser.Name = "txtUser"
        Me.txtUser.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtUser.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtUser.SelectedText = ""
        Me.txtUser.SelectionLength = 0
        Me.txtUser.SelectionStart = 0
        Me.txtUser.ShortcutsEnabled = True
        Me.txtUser.Size = New System.Drawing.Size(120, 23)
        Me.txtUser.TabIndex = 2
        Me.txtUser.UseSelectable = True
        Me.txtUser.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtUser.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'lstAccounts
        '
        Me.lstAccounts.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.lstAccounts.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstAccounts.FormattingEnabled = True
        Me.lstAccounts.Location = New System.Drawing.Point(13, 100)
        Me.lstAccounts.Name = "lstAccounts"
        Me.lstAccounts.Size = New System.Drawing.Size(142, 199)
        Me.lstAccounts.TabIndex = 34
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.White
        Me.TabPage2.Controls.Add(Me.GroupBox2)
        Me.TabPage2.Controls.Add(Me.GroupBox1)
        Me.TabPage2.Location = New System.Drawing.Point(140, 4)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(534, 362)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Point Ranges"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.RBtn_Cancel)
        Me.GroupBox2.Controls.Add(Me.txtBronze)
        Me.GroupBox2.Controls.Add(Me.txtSilver)
        Me.GroupBox2.Controls.Add(Me.txtGold)
        Me.GroupBox2.Controls.Add(Me.MetroLabel11)
        Me.GroupBox2.Controls.Add(Me.MetroLabel10)
        Me.GroupBox2.Controls.Add(Me.MetroLabel9)
        Me.GroupBox2.Controls.Add(Me.RBtn_Edit)
        Me.GroupBox2.Controls.Add(Me.RBtn_Save)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 135)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(493, 205)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        '
        'RBtn_Cancel
        '
        Me.RBtn_Cancel.Location = New System.Drawing.Point(403, 168)
        Me.RBtn_Cancel.Name = "RBtn_Cancel"
        Me.RBtn_Cancel.Size = New System.Drawing.Size(75, 23)
        Me.RBtn_Cancel.TabIndex = 46
        Me.RBtn_Cancel.Text = "Cancel"
        Me.RBtn_Cancel.UseSelectable = True
        '
        'txtBronze
        '
        '
        '
        '
        Me.txtBronze.CustomButton.Image = Nothing
        Me.txtBronze.CustomButton.Location = New System.Drawing.Point(150, 1)
        Me.txtBronze.CustomButton.Name = ""
        Me.txtBronze.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.txtBronze.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtBronze.CustomButton.TabIndex = 1
        Me.txtBronze.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtBronze.CustomButton.UseSelectable = True
        Me.txtBronze.CustomButton.Visible = False
        Me.txtBronze.Enabled = False
        Me.txtBronze.Lines = New String(-1) {}
        Me.txtBronze.Location = New System.Drawing.Point(95, 123)
        Me.txtBronze.MaxLength = 32767
        Me.txtBronze.Name = "txtBronze"
        Me.txtBronze.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBronze.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtBronze.SelectedText = ""
        Me.txtBronze.SelectionLength = 0
        Me.txtBronze.SelectionStart = 0
        Me.txtBronze.ShortcutsEnabled = True
        Me.txtBronze.Size = New System.Drawing.Size(172, 23)
        Me.txtBronze.TabIndex = 43
        Me.txtBronze.UseSelectable = True
        Me.txtBronze.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtBronze.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'txtSilver
        '
        '
        '
        '
        Me.txtSilver.CustomButton.Image = Nothing
        Me.txtSilver.CustomButton.Location = New System.Drawing.Point(150, 1)
        Me.txtSilver.CustomButton.Name = ""
        Me.txtSilver.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.txtSilver.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtSilver.CustomButton.TabIndex = 1
        Me.txtSilver.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtSilver.CustomButton.UseSelectable = True
        Me.txtSilver.CustomButton.Visible = False
        Me.txtSilver.Enabled = False
        Me.txtSilver.Lines = New String(-1) {}
        Me.txtSilver.Location = New System.Drawing.Point(95, 70)
        Me.txtSilver.MaxLength = 32767
        Me.txtSilver.Name = "txtSilver"
        Me.txtSilver.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSilver.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtSilver.SelectedText = ""
        Me.txtSilver.SelectionLength = 0
        Me.txtSilver.SelectionStart = 0
        Me.txtSilver.ShortcutsEnabled = True
        Me.txtSilver.Size = New System.Drawing.Size(172, 23)
        Me.txtSilver.TabIndex = 42
        Me.txtSilver.UseSelectable = True
        Me.txtSilver.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtSilver.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'txtGold
        '
        '
        '
        '
        Me.txtGold.CustomButton.Image = Nothing
        Me.txtGold.CustomButton.Location = New System.Drawing.Point(150, 1)
        Me.txtGold.CustomButton.Name = ""
        Me.txtGold.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.txtGold.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtGold.CustomButton.TabIndex = 1
        Me.txtGold.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtGold.CustomButton.UseSelectable = True
        Me.txtGold.CustomButton.Visible = False
        Me.txtGold.Enabled = False
        Me.txtGold.Lines = New String(-1) {}
        Me.txtGold.Location = New System.Drawing.Point(95, 19)
        Me.txtGold.MaxLength = 32767
        Me.txtGold.Name = "txtGold"
        Me.txtGold.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtGold.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtGold.SelectedText = ""
        Me.txtGold.SelectionLength = 0
        Me.txtGold.SelectionStart = 0
        Me.txtGold.ShortcutsEnabled = True
        Me.txtGold.Size = New System.Drawing.Size(172, 23)
        Me.txtGold.TabIndex = 41
        Me.txtGold.UseSelectable = True
        Me.txtGold.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtGold.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel11
        '
        Me.MetroLabel11.AutoSize = True
        Me.MetroLabel11.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel11.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.MetroLabel11.Location = New System.Drawing.Point(16, 121)
        Me.MetroLabel11.Name = "MetroLabel11"
        Me.MetroLabel11.Size = New System.Drawing.Size(72, 25)
        Me.MetroLabel11.TabIndex = 40
        Me.MetroLabel11.Text = "Bronze"
        '
        'MetroLabel10
        '
        Me.MetroLabel10.AutoSize = True
        Me.MetroLabel10.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel10.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.MetroLabel10.Location = New System.Drawing.Point(16, 70)
        Me.MetroLabel10.Name = "MetroLabel10"
        Me.MetroLabel10.Size = New System.Drawing.Size(59, 25)
        Me.MetroLabel10.TabIndex = 39
        Me.MetroLabel10.Text = "Silver"
        '
        'MetroLabel9
        '
        Me.MetroLabel9.AutoSize = True
        Me.MetroLabel9.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel9.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.MetroLabel9.Location = New System.Drawing.Point(16, 17)
        Me.MetroLabel9.Name = "MetroLabel9"
        Me.MetroLabel9.Size = New System.Drawing.Size(52, 25)
        Me.MetroLabel9.TabIndex = 38
        Me.MetroLabel9.Text = "Gold"
        '
        'RBtn_Edit
        '
        Me.RBtn_Edit.Location = New System.Drawing.Point(310, 168)
        Me.RBtn_Edit.Name = "RBtn_Edit"
        Me.RBtn_Edit.Size = New System.Drawing.Size(75, 23)
        Me.RBtn_Edit.TabIndex = 44
        Me.RBtn_Edit.Text = "Edit"
        Me.RBtn_Edit.UseSelectable = True
        '
        'RBtn_Save
        '
        Me.RBtn_Save.Location = New System.Drawing.Point(310, 168)
        Me.RBtn_Save.Name = "RBtn_Save"
        Me.RBtn_Save.Size = New System.Drawing.Size(75, 23)
        Me.RBtn_Save.TabIndex = 45
        Me.RBtn_Save.Text = "Save"
        Me.RBtn_Save.UseSelectable = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.MetroLabel8)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(223, 107)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Help"
        '
        'MetroLabel8
        '
        Me.MetroLabel8.AutoSize = True
        Me.MetroLabel8.Location = New System.Drawing.Point(6, 16)
        Me.MetroLabel8.Name = "MetroLabel8"
        Me.MetroLabel8.Size = New System.Drawing.Size(213, 57)
        Me.MetroLabel8.TabIndex = 0
        Me.MetroLabel8.Text = "Ranges for awards are shown here." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "To change the ranges, click Edit."
        '
        'tilHome
        '
        Me.tilHome.ActiveControl = Nothing
        Me.tilHome.Location = New System.Drawing.Point(688, 13)
        Me.tilHome.Name = "tilHome"
        Me.tilHome.Size = New System.Drawing.Size(75, 75)
        Me.tilHome.Style = MetroFramework.MetroColorStyle.Pink
        Me.tilHome.TabIndex = 36
        Me.tilHome.Text = "Home"
        Me.tilHome.TileImage = CType(resources.GetObject("tilHome.TileImage"), System.Drawing.Image)
        Me.tilHome.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.tilHome.UseSelectable = True
        Me.tilHome.UseTileImage = True
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.ceuratingforms.My.Resources.Resources.ceu_sao
        Me.PictureBox2.Location = New System.Drawing.Point(574, 13)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(77, 77)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 37
        Me.PictureBox2.TabStop = False
        '
        'frmAdAccounts
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(771, 578)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.CustomTab1)
        Me.Controls.Add(Me.tilHome)
        Me.Controls.Add(Me.MetroLabel7)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.MetroLabel3)
        Me.Controls.Add(Me.MetroLabel2)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmAdAccounts"
        Me.Resizable = False
        Me.Style = MetroFramework.MetroColorStyle.Pink
        Me.Text = "Centro Escolar University"
        Me.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CustomTab1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.MetroPanel1.ResumeLayout(False)
        Me.MetroPanel1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents MetroLabel7 As MetroFramework.Controls.MetroLabel
    Friend WithEvents tipError As MetroFramework.Components.MetroToolTip
    Friend WithEvents CustomTab1 As CustomTab
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents tilAdd As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroPanel1 As MetroFramework.Controls.MetroPanel
    Friend WithEvents MetroLabel6 As MetroFramework.Controls.MetroLabel
    Friend WithEvents btnCancel As MetroFramework.Controls.MetroButton
    Friend WithEvents btnSave As MetroFramework.Controls.MetroButton
    Friend WithEvents btnDelete As MetroFramework.Controls.MetroButton
    Friend WithEvents btnEdit As MetroFramework.Controls.MetroButton
    Friend WithEvents MetroLabel5 As MetroFramework.Controls.MetroLabel
    Friend WithEvents comType As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroLabel4 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents txtPassword As MetroFramework.Controls.MetroTextBox
    Friend WithEvents txtUser As MetroFramework.Controls.MetroTextBox
    Friend WithEvents lstAccounts As ListBox
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents RBtn_Cancel As MetroFramework.Controls.MetroButton
    Friend WithEvents txtBronze As MetroFramework.Controls.MetroTextBox
    Friend WithEvents txtSilver As MetroFramework.Controls.MetroTextBox
    Friend WithEvents txtGold As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel11 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel10 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel9 As MetroFramework.Controls.MetroLabel
    Friend WithEvents RBtn_Edit As MetroFramework.Controls.MetroButton
    Friend WithEvents RBtn_Save As MetroFramework.Controls.MetroButton
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents MetroLabel8 As MetroFramework.Controls.MetroLabel
    Friend WithEvents tilHome As MetroFramework.Controls.MetroTile
    Friend WithEvents PictureBox2 As PictureBox
End Class
