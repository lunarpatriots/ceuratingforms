﻿Public Class frmAdAccount

    Dim previousForm As Form

    Public Sub init(ByVal form As Form)
        previousForm = form
    End Sub

    Private Sub FrmAdHome_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Me.Closing
        If MsgBox("Are you sure you want to exit?", MsgBoxStyle.YesNo, "") = MsgBoxResult.Yes Then
            End
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub AddAccount_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtUsername.Select()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        previousForm.Enabled = True
        previousForm.Select()
        Hide()
    End Sub

    Private Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        clear()
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        If String.IsNullOrEmpty(txtUsername.Text) Then
            tipError.Show("Please eneter a username.", txtUsername)
            txtUsername.Select()
        ElseIf String.IsNullOrEmpty(txtPassword.Text) Then
            tipError.Show("Please enter a password.", txtPassword)
            txtPassword.Select()
        ElseIf String.IsNullOrEmpty(txtRepass.Text) Then
            tipError.Show("Please retype your password.", txtRepass)
            txtRepass.Select()
        ElseIf comType.SelectedIndex = -1 Then
            tipError.Show("Please choose an account type.", comType)
            comType.Select()
        Else
            Dim username As String = txtUsername.Text
            Dim password As String = txtPassword.Text
            Dim confirm As String = txtRepass.Text
            Dim type As String = comType.SelectedItem.ToString

            If password <> confirm Then
                tipError.Show("Passwords do not match!", txtPassword)
            Else
                Dim check = "SELECT Username FROM userinfo WHERE Username = '" & username & "'"
                If count(check, 1) <> 0 Then
                    tipError.Show("Username already taken!", txtUsername)
                    txtUsername.Select()
                Else
                    Dim query = "INSERT INTO userinfo VALUES ('" & username & "' , '" & confirm & "' , '" & type & "')"
                    If ExecuteQuery(query) Then
                        MsgBox("Account created!")
                        Clear()
                    Else
                        MsgBox("Failed to add account.")
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub Clear()
        txtUsername.Text = String.Empty
        txtPassword.Text = String.Empty
        txtRepass.Text = String.Empty
        comType.SelectedIndex = -1
    End Sub
End Class