﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class studentReport
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(studentReport))
        Me.MetroLabel8 = New MetroFramework.Controls.MetroLabel()
        Me.cmb_studentNames = New MetroFramework.Controls.MetroComboBox()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.tilHome = New MetroFramework.Controls.MetroTile()
        Me.tilBack = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.StudSums = New ceuratingforms.StudSums()
        Me.allrfBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.allrfTableAdapter = New ceuratingforms.StudSumsTableAdapters.allrfTableAdapter()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StudSums, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.allrfBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MetroLabel8
        '
        Me.MetroLabel8.AutoSize = True
        Me.MetroLabel8.Location = New System.Drawing.Point(21, 121)
        Me.MetroLabel8.Name = "MetroLabel8"
        Me.MetroLabel8.Size = New System.Drawing.Size(139, 19)
        Me.MetroLabel8.TabIndex = 39
        Me.MetroLabel8.Text = "Search Student Name:"
        '
        'cmb_studentNames
        '
        Me.cmb_studentNames.FormattingEnabled = True
        Me.cmb_studentNames.ItemHeight = 23
        Me.cmb_studentNames.Location = New System.Drawing.Point(166, 117)
        Me.cmb_studentNames.Name = "cmb_studentNames"
        Me.cmb_studentNames.Size = New System.Drawing.Size(216, 29)
        Me.cmb_studentNames.Style = MetroFramework.MetroColorStyle.Pink
        Me.cmb_studentNames.TabIndex = 38
        Me.cmb_studentNames.UseSelectable = True
        Me.cmb_studentNames.UseStyleColors = True
        '
        'ReportViewer1
        '
        Me.ReportViewer1.DocumentMapWidth = 27
        ReportDataSource1.Name = "DataSet1"
        ReportDataSource1.Value = Me.allrfBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource1)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "ceuratingforms.Report1.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(4, 152)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.Size = New System.Drawing.Size(681, 332)
        Me.ReportViewer1.TabIndex = 40
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.ceuratingforms.My.Resources.Resources.ceu_sao
        Me.PictureBox2.Location = New System.Drawing.Point(520, 16)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(77, 77)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 43
        Me.PictureBox2.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.ceuratingforms.My.Resources.Resources.ceu
        Me.PictureBox1.Location = New System.Drawing.Point(85, 16)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(77, 77)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 42
        Me.PictureBox1.TabStop = False
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel2.Location = New System.Drawing.Point(258, 49)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(172, 25)
        Me.MetroLabel2.TabIndex = 41
        Me.MetroLabel2.Text = "Student Affairs Office"
        Me.MetroLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'tilHome
        '
        Me.tilHome.ActiveControl = Nothing
        Me.tilHome.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.tilHome.Location = New System.Drawing.Point(601, 490)
        Me.tilHome.Name = "tilHome"
        Me.tilHome.Size = New System.Drawing.Size(75, 75)
        Me.tilHome.Style = MetroFramework.MetroColorStyle.Pink
        Me.tilHome.TabIndex = 46
        Me.tilHome.Text = "Home"
        Me.tilHome.TileImage = Global.ceuratingforms.My.Resources.Resources.ic_home_white_48dp_1x
        Me.tilHome.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.tilHome.UseSelectable = True
        Me.tilHome.UseTileImage = True
        '
        'tilBack
        '
        Me.tilBack.ActiveControl = Nothing
        Me.tilBack.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.tilBack.Location = New System.Drawing.Point(520, 490)
        Me.tilBack.Name = "tilBack"
        Me.tilBack.Size = New System.Drawing.Size(75, 75)
        Me.tilBack.Style = MetroFramework.MetroColorStyle.Pink
        Me.tilBack.TabIndex = 47
        Me.tilBack.Text = "Back"
        Me.tilBack.TileImage = Global.ceuratingforms.My.Resources.Resources.ic_arrow_back_white_48dp_1x
        Me.tilBack.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.tilBack.UseSelectable = True
        Me.tilBack.UseTileImage = True
        '
        'MetroLabel1
        '
        Me.MetroLabel1.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel1.Location = New System.Drawing.Point(214, 578)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(260, 20)
        Me.MetroLabel1.TabIndex = 48
        Me.MetroLabel1.Text = "©BSIT3A 2016"
        Me.MetroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'StudSums
        '
        Me.StudSums.DataSetName = "StudSums"
        Me.StudSums.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'allrfBindingSource
        '
        Me.allrfBindingSource.DataMember = "allrf"
        Me.allrfBindingSource.DataSource = Me.StudSums
        '
        'allrfTableAdapter
        '
        Me.allrfTableAdapter.ClearBeforeFill = True
        '
        'studentReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(688, 606)
        Me.Controls.Add(Me.MetroLabel1)
        Me.Controls.Add(Me.tilBack)
        Me.Controls.Add(Me.tilHome)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.MetroLabel2)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Controls.Add(Me.MetroLabel8)
        Me.Controls.Add(Me.cmb_studentNames)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "studentReport"
        Me.Resizable = False
        Me.Style = MetroFramework.MetroColorStyle.Pink
        Me.Text = "CENTRO ESCOLAR UNIVERSITY"
        Me.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StudSums, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.allrfBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MetroLabel8 As MetroFramework.Controls.MetroLabel
    Friend WithEvents cmb_studentNames As MetroFramework.Controls.MetroComboBox
    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents tilHome As MetroFramework.Controls.MetroTile
    Friend WithEvents tilBack As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents allrfBindingSource As BindingSource
    Friend WithEvents StudSums As StudSums
    Friend WithEvents allrfTableAdapter As StudSumsTableAdapters.allrfTableAdapter
End Class
