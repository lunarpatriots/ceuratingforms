﻿Imports System.Collections.Generic

Public Class frmLogin

    Dim count As Integer


    Private Sub frmLogin_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtUsername.Select()
    End Sub

    Private Sub FrmAdHome_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Me.Closing
        If MsgBox("Are you sure you want to exit?", MsgBoxStyle.YesNo, "") = MsgBoxResult.Yes Then
            End
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.Click
        Dim strUser As String = txtUsername.Text
        Dim strPass As String = txtPassword.Text

        Dim query As String = "SELECT * FROM userinfo WHERE username = '" & strUser & "' AND psword = '" & strPass & "';"

        Dim temp As ArrayList = RetrieveQuery(query, 3)

        If String.IsNullOrEmpty(strUser) Then
            tipError.Show("Please enter a username.", txtUsername)
            txtUsername.Select()
        ElseIf String.IsNullOrEmpty(strPass) Then
            tipError.Show("Please enter a password.", txtPassword)
            txtPassword.Select()
        Else
            If temp.Count = 1 Then
                Dim username = temp(0)(0).ToString()
                Dim access = temp(0)(2).ToString()

                SetAccount(username)
                SetAccess(access)

                Dim nextForm As Form

                nextForm = New frmAdHome()
                nextForm.Show()
                Hide()
            Else
                If count < 2 Then
                    count += 1
                    txtUsername.Text = String.Empty
                    txtPassword.Text = String.Empty
                    MsgBox("Invalid Username/password")
                    txtUsername.Select()
                Else
                    MsgBox("You have reached maximum attempts. System will now close.")
                    End
                End If
            End If
        End If
    End Sub
End Class